package br.com.moro.rabbitmq.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class AcessoMensagemBean implements Serializable {

	private static final long serialVersionUID = 6284803847654897269L;

	private String texto;
	private Date data;
    private int prioridade;
    private boolean segredo;

    public AcessoMensagemBean(@JsonProperty("text") String texto,
                         @JsonProperty("priority") int prioridade,
                         @JsonProperty("secret") boolean segredo,
                         @JsonProperty("date") Date data) {
        this.texto = texto;
        this.prioridade = prioridade;
        this.segredo = segredo;
        this.setData(data);
    }

	public String getTexto() {
		return texto;
	}

	public int getPrioridade() {
		return prioridade;
	}

	public boolean isSegredo() {
		return segredo;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "AcessoMensagem [texto=" + texto + ", data=" + data + ", prioridade=" + prioridade + ", segredo="
				+ segredo + "]";
	}
}
