package br.com.moro.rabbitmq.listener;


import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import br.com.moro.rabbitmq.AcessoMensagemApplication;
import br.com.moro.rabbitmq.bean.AcessoMensagemBean;

@Service
public class AcessoMensagemListener {

    private static final Logger log = LoggerFactory.getLogger(AcessoMensagemListener.class);

    @RabbitListener(queues = AcessoMensagemApplication.QUEUE_GENERIC_NAME)
    public void mensagemGenerica(final Message message) {
        log.info("Mensagem Generica: {}", message.toString());
    }

    @RabbitListener(queues = AcessoMensagemApplication.QUEUE_SPECIFIC_NAME)
    public void mensagemDeAcesso(final AcessoMensagemBean customMessage) {
        log.info(new Date() + ":Mensagem: {}", customMessage.toString());
    }

    @RabbitListener(queues = "XXXXX")
    public void mensagemDeAcessosss(final AcessoMensagemBean customMessage) {
        log.info(new Date() + ":XXXXX: {}", customMessage.toString());
    }
}
