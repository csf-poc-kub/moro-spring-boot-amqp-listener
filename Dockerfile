FROM openjdk:8-jdk-alpine

MAINTAINER patrickmoro@gmail.com

RUN mkdir /app

WORKDIR /app

ADD target/moro-spring-boot-amqp-listener-1.0.0.jar listener.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/listener.jar"]

# docker network create docker_network_fila_com

# docker build -t moro-spring-boot-listener:0.1 -t moro-spring-boot-listener:latest .

# docker run --net docker_network_fila_com -d --hostname moro-spring-boot-listener --name moro-spring-boot-listener moro-spring-boot-listener:0.1

# docker stop moro-spring-boot-listener

# docker rm moro-spring-boot-listener