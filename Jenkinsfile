node {
    def mvnHome = tool 'maven-3.5.2'
    def dockerImage
    def dockerRepoUrl = "https://registry.hub.docker.com"
    def gitUrl = "https://gitlab.com/csf-poc-kub/moro-spring-boot-amqp-listener.git"
    def dockerImageName = "patrickmoro/moro-spring-boot-amqp-listener"
    def imageTag = "1.0.0"
    def dockerImageTag = "${dockerImageName}:${imageTag}"
    def branchName = "develop"
    def credentialGit = 'bitbucket_cred'
    def credentialDocker = 'DOCKER_HUB'

    stage('Clone Repo') {
      git credentialsId: credentialGit, poll: false, branch: branchName, url: gitUrl
      mvnHome = tool 'maven-3.5.2'
    }

    stage('Build Project') {
      sh "'${mvnHome}/bin/mvn' -Dmaven.test.failure.ignore clean package"
    }

    stage('Build Docker Image') {
      dockerImage = docker.build(dockerImageTag)
    }

    stage('Docker Login/Tag'){
      echo "Docker Image Tag Name: ${dockerImageTag} - ${dockerRepoUrl}"
      withCredentials([usernamePassword(credentialsId: credentialDocker, passwordVariable: 'docker_pass', usernameVariable: 'docker_user')]) {
		sh "docker login -u $docker_user -p $docker_pass ${dockerRepoUrl}"
      }
      sh "docker tag ${dockerImageTag} ${dockerImageTag}"
    }

    stage('Push Docker Image'){
      docker.withRegistry(dockerRepoUrl, credentialDocker) {
          dockerImage.push()
      }
    }
}